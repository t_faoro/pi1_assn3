// *** CPSC 4210/5210 Assignment 3 code
// *** main file for the packet construction test cases

#include <cppunit/ui/text/TestRunner.h>
#include "ConcreteIPUDPPacketFactory.h"

// Include the test fixture header files here
#include "TestIPUDPPacketPi1.h"
#include "TestOLSRPacketPi1.h"
#include "TestOLSRHelloPi1.h"


// Initialize the static factory member of the test fixtures here
std::shared_ptr<IPUDPPacketFactory> TestIPUDPPacketPi1::factory =
	std::make_shared<ConcreteIPUDPPacketFactory>();
std::shared_ptr<IPUDPPacketFactory> TestOLSRPacketPi1::factory =
	std::make_shared<ConcreteIPUDPPacketFactory>();
std::shared_ptr<IPUDPPacketFactory> TestOLSRHelloPi1::factory =
	std::make_shared<ConcreteIPUDPPacketFactory>();

int main() {
	CppUnit::TextUi::TestRunner runner;
	runner.addTest(TestIPUDPPacketPi1::suite());
	runner.addTest(TestOLSRPacketPi1::suite());
	runner.addTest(TestOLSRHelloPi1::suite());
	// add other test suites here like on the line above
	runner.run();
	return 0;
}
