// *** CPSC 4210/5210 Assignment 3 code
// ***** IPUDPPacket interface: creates the combined header of IP/UDP packed

#ifndef __IPUDPPACKETSAMPLE_H
#define __IPUDPPACKETSAMPLE_H

#include <cstring>
#include "IPUDPPacket.h"

class IPUDPPacketSample : public IPUDPPacket {
public:
	// Can provide any constructors needed; the sample relies on the
	// default constructor supplied by the compiler

	// concrete implementation for the header routine
	int setHeader(unsigned char * buf, int buflen, IPAddr src,
								IPAddr dest, int TTL, int datalen) {
		// we don't do much, just fill the data length part of the header,
		// as demo.

		// first check if the buffer is big enough
		if (buflen < datalen+28)
			return -1;
		
		std::memset(buf, 0, buflen);  // we clear the buffer first!
		*(reinterpret_cast<uint16_t*>(buf+2)) = 28+datalen;  // set the packet
																										// length field
		return 28;  // the size of the header in bytes
	}
};

#endif
