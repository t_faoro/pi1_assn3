#ifndef __TESTOLSRHELLOPI1_H
#define __TESTOLSRHELLOPI1_H

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <cstdlib>
#include <iostream>

#include "IPUDPPacketFactory.h"
#include "IPAddr.h"


/// Test fixture for ProgressiveRateStrategy
class TestOLSRHelloPi1 : public CppUnit::TestFixture {
	CPPUNIT_TEST_SUITE(TestOLSRHelloPi1);
	CPPUNIT_TEST(sampleTestIPUDPDataTooLarge);
	CPPUNIT_TEST_SUITE_END();
private:
	// we make the factory static so that we can initialize it in the
	// .cc file to the concrete factory that returns the concrete object
	// with our implementation of setHeader()
	static std::shared_ptr<IPUDPPacketFactory> factory;

private:
	// the packet object being tested
	std::shared_ptr<IPUDPPacket> ipudp;
	// the buffer used for testing
	unsigned char * buffer;
	// the buffer length
	const int buflen = 1000;
	

public:
	void setUp()  {
		if (factory)
			ipudp = factory->createIPUDPPacket();
		else {
	    std::cerr << "TestIPUDPSample test fixture not properly initialized!\n";
			exit(EXIT_FAILURE);
		}
		buffer = new unsigned char [buflen];
	};
	
	void tearDown()  {
		delete[] buffer;
		// we don't need to delete ipudp since it is a smart pointer
	};

	// *************** test cases ************
	void sampleTestIPUDPDataTooLarge() {
		IPAddr src,dst;
		int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 11);
		CPPUNIT_ASSERT(header_len == -1);
	}
};

#endif